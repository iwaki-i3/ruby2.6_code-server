FROM ruby:2.6

# Install code-server
RUN wget https://github.com/cdr/code-server/releases/download/1.1156-vsc1.33.1/code-server1.1156-vsc1.33.1-linux-x64.tar.gz ; \
    tar xzvf code-server1.1156-vsc1.33.1-linux-x64.tar.gz ; \
    mv code-server1.1156-vsc1.33.1-linux-x64/code-server /usr/local/bin/code-server ; \
    rm -rf code-server1.1156-vsc1.33.1-linux-x64/ code-server1.1156-vsc1.33.1-linux-x64.tar.gz ; chmod 755 /usr/local/bin/code-server

RUN set -x && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
      less \
      nano \
      sudo && \
    rm -rf /var/cache/apt/archives/* /var/lib/apt/lists/*

# for code-server.
EXPOSE 8443

# 一般ユーザに降格させる。
RUN useradd -m debian -s /bin/bash
RUN mkdir -p /usr/src/app && chown debian:debian /usr/src/app
RUN echo "debian ALL=(ALL:ALL) NOPASSWD: ALL" | tee /etc/sudoers.d/debian_nopasswd
USER debian
WORKDIR /usr/src/app

CMD ["code-server", "--allow-http", "--no-auth"]
